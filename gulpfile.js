const gulp = require('gulp');
const gutil = require('gulp-util');
const cond = require('gulp-cond');
const del = require('del');
const {argv} = require('yargs');
const runSequence = require('run-sequence');

const merge = require('merge-stream');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

const eslint = require('gulp-eslint');

const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
const cssnano = require('cssnano');

const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

const watchify = require('watchify');
const browserify = require('browserify');
const hmr = require('browserify-hmr');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const replace = require('gulp-replace');
const childprocess = require('child_process');


// If gulp was called in the terminal with the --prod flag, set the node environment to production
if (argv.prod) {
  process.env.NODE_ENV = 'production';
}
// If gulp was called in the terminal with the --build flag, do not start server

if (argv.build) {
  process.env.BUILD = 'build';
}
const PROD = process.env.NODE_ENV === 'production';
const BUILD = process.env.BUILD === 'build';
const HASH = '';//childprocess.execSync('git rev-parse --short HEAD').toString().replace(/^\s+|\s+$/g, '');
const BRANCH = '';//childprocess.execSync('git rev-parse --abbrev-ref HEAD').toString().replace(/^\s+|\s+$/g, '');

console.log('branch:', BRANCH);
console.log('commit:', HASH);
console.log(PROD);

// Configuration
const src = 'src';
const config = {
  port: PROD ? 443 : 3000,
  env: 'env.php',
  title: PROD ? 'D4R14' : 'D4R14 BETA',
  branch: BRANCH,
  hash: HASH,
  paths: {
    baseDir: 'public',
    configDir: 'config',
    js: src + '/js/**/*.js',
    css: src + '/scss/**/*.scss',
    index: 'public/index.php'
  }
};

/**
 * Gulp Tasks
 **/

// clears the contents of the dist and build folder
gulp.task('clean', function () {
  return del(['public/css/*', 'public/js/*']);
});

// linting
gulp.task('lint', function () {
  return gulp.src(config.paths.js)
    .pipe(eslint({
      fix: true
    }))
    .pipe(eslint.format())
});

// sass
gulp.task('css', function () {
  let processors = [
    autoprefixer({
      browsers: ['last 2 versions', '> 5%', 'Firefox ESR', 'Safari >= 8'],
      cascade: false
    }),
    mqpacker({
      sort: true
    }),
    cssnano
  ];

  return gulp.src(config.paths.css)
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(cond(!PROD, sourcemaps.init({loadMaps: true})))
    .pipe(cond(!PROD, sourcemaps.write('./')))
    .pipe(gulp.dest(config.paths.baseDir + '/css'));

});

// bundles our JS using browserify. Sourcemaps are used in development, while minification is used in production.
gulp.task('app-js', function () {
  let b = browserify({
    entries: src + '/js/main.js',
    debug: true,
    plugin: BUILD ? [] : [watchify],
    cache: {},
    packageCache: {},
    bundleExternal: true
  });
  return b.bundle()
    .on('error', function (err) {
      // print the error (can replace with gulp-util)
      console.log(err.message);
      // end this stream
      this.emit('end');
    })
    .pipe(source('js/main.js'))
    .pipe(buffer())
    .pipe(cond(PROD, uglify()))
    .pipe(cond(!PROD, sourcemaps.init({loadMaps: true})))
    .pipe(cond(!PROD, sourcemaps.write('./')))
    .pipe(gulp.dest(config.paths.baseDir));
});

// watch for change & reload
gulp.task('watch', function () {
  browserSync.init({
    proxy: 'd4r14.local',
    port: config.port
  });
  gulp.watch(config.paths.css, ['css']);
  gulp.watch(config.paths.css).on('change', browserSync.reload);
  gulp.watch(config.paths.js, ['app-js']);
  gulp.watch(config.paths.js).on('change', browserSync.reload);
  gulp.watch(config.paths.index).on('change', browserSync.reload);
});

// copy files
gulp.task('copy', function () {
    gulp
      .src('node_modules/waypoints/lib/noframework.waypoints.js')
      .pipe(gulp.dest(config.paths.baseDir + '/js'));
    gulp
      .src(src + '/htaccess/' + (PROD ? 'prod' : 'beta') + '/.htaccess')
      .pipe(gulp.dest(config.paths.baseDir));
  }
);

// replace env strings upon build
gulp.task('replace', function () {
  gulp
    .src(config.env)
    .pipe(replace('{{TITLE}}', config.title))
    .pipe(replace('{{APP_URL}}', config.appUrl))
    .pipe(replace('{{BRANCH}}', config.branch))
    .pipe(replace('{{HASH}}', config.hash))
    .pipe(replace('\'{{BETA}}\'', !PROD))
    .pipe(gulp.dest(config.paths.configDir));
});

// default task, bundles the entire app and hosts it on an Express server
gulp.task('default', function (cb) {
  if (BUILD) {
    runSequence('clean', 'lint', 'css', 'app-js', 'copy', 'replace');
  } else {
    runSequence('clean', 'lint', 'css', 'app-js', 'copy', 'replace', 'watch', cb);
  }
});

