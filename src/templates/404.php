<main class="err404">
    <div class="top-wrapper">
        <div class="va-middle">
            <div class="text-center">
                <a href="/"><img src="/img/logo_d4r14.png" alt="D4R14 logo"></a>
                <h1>404</h1>
                <h2>Nie znaleziono strony</h2>
                <a href="/" class="button-small">Powrót na stronę główną</a>
            </div>
        </div>
    </div>
</main>