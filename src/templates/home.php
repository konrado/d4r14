<div class="top-wrapper">
    <div class="wrapper va-middle animate">
        <div>
            <h1 class="with-anim">Po raz pierwszy w&nbsp;Polsce</h1>
            <div class="logos">
                <div id="atelier">
                    <img src="/img/logo_atelierluxus.svg" alt="Atelier Luxus" class="with-anim delayed-2">
                </div>
                <div id="luxanov">
                    <img src="/img/logo_luxanov.svg" alt="Luxanov" class="with-anim delayed-4">
                </div>
                <!-- <div id="edges">
                    <img src="/img/logo_edges.png" alt="Edges of Luxus" class="with-anim delayed-6">
                </div> -->
            </div>
        </div>
    </div>
</div>

<div class="group img-left waypoint">
    <div class="wrapper va-middle">
        <div class="image with-anim">
            <div>
                <img src="/img/pic1.png" alt="">
            </div>
        </div>
        <div class="text with-anim delayed-4">
            <p>Ręcznie wykonane wzory pozwalające wyrazić piękno w&nbsp;najdrobniejszych szczegółach.</p>
        </div>
    </div>
</div>
<div class="group img-right waypoint">
    <div class="wrapper va-middle">
        <div class="image with-anim">
            <div>
                <img src="/img/pic2.png" alt="">
            </div>
        </div>
        <div class="text with-anim delayed-4">
            <p>Nieskończona liczba kombinacji przełączników i&nbsp;gniazd.</p>
        </div>
    </div>
</div>
<div class="group img-left waypoint">
    <div class="wrapper va-middle">
        <div class="image with-anim">
            <div>
                <img src="/img/pic3.png" alt="">
            </div>
        </div>
        <div class="text with-anim delayed-4">
            <p>Twoje własne, spersonalizowane urządzenie w&nbsp;połączeniu z&nbsp;ponadczasowym pięknem minimalizmu.</p>
        </div>
    </div>
</div>
<div class="group img-right waypoint">
    <div class="wrapper va-middle">
        <div class="image with-anim">
            <div>
                <img src="/img/pic4.png" alt="">
            </div>
        </div>
        <div class="text with-anim delayed-4">
            <p>Zaprojektowane, aby się wyróżniać.<br>Na zawsze.</p>
        </div>
    </div>
</div>

<div class="contact waypoint">
    <div class="wrapper">
        <div class="text with-anim">
            <h2>Skontaktuj się z&nbsp;nami</h2>
        </div>
        <div class="with-anim logo delayed-2">
            <div>
                <img src="/img/logo_d4r14.png" alt="">
            </div>
        </div>
        <div class="links">
            <a href="tel:+48511753146" class="with-anim delayed-4">+48 511 75 31 46</a><br>
            <a href="mailto:info@d4r14.pl" class="with-anim delayed-5">info@d4r14.pl</a>
        </div>
        <div class="image with-anim delayed-7">
            <img src="/img/pic5.png" alt="logo D4R14">
        </div>
    </div>
</div>
