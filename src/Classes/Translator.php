<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 09.03.2018
 * Time: 21:19
 */

namespace ES\Classes;

use Exception;

class Translator
{
  protected $currentLang;
  protected $translations;

  public function __construct(array $config, array $currentLang)
  {
    $this->currentLang = $currentLang;
    $this->loadTranslations();
  }

  public function tr(string $phrase): string
  {
    if (array_key_exists($phrase, $this->translations)) {
      return $this->translations[$phrase];
    } else {
      return '!!NO TRANS!!';
    }
  }

  public function _tr(string $phrase)
  {
    echo $this->tr($phrase);
  }

  protected function loadTranslations()
  {
    $langFile = __DIR__ . '/../../config/translations/' . $this->currentLang['short'] . '.php';
    if (file_exists($langFile)) {
      $this->translations = include_once $langFile;
    } else {
      throw new Exception('no language translations');
    }
  }
}