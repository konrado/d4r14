<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 26.02.2018
 * Time: 21:48
 */

namespace ES\Classes;

use Exception;

class PathLoader
{
  protected $paths;
  protected $languages;
  protected $currentLang;
  protected $pathParts;
  protected $currentRoute;

  const TEMPLATES_DIR = __DIR__ . '/../templates/';
  const PARTS_DIR = __DIR__ . '/../commonParts/';

  public function __construct(array $config, array $languages, array $currentLang, array $pathParts)
  {
    if (array_key_exists('paths', $config)) {
      $this->paths = $config['paths'];
    } else {
      throw new Exception('paths missing in config file');
    }
    $this->languages = $languages;
    $this->currentLang = $currentLang;
    $this->pathParts = $pathParts;

    $fullPath = implode('/', $this->pathParts);
    $this->currentRoute = '404';
    foreach ($this->paths as $routeName => $path) {
      if ($path['routes'][$this->currentLang['short']] === $fullPath) {
        $this->currentRoute = $routeName;
      }
    }
  }

  public function getLanguageLinks()
  {
    $links = array();
    foreach ($this->languages as $lang) {
      $links[] = [
        'route' => $lang['route'],
        'label' => $lang['label']
      ];
    }

    return $links;
  }

  public function getCurrentRoute() {
    return $this->currentRoute;
  }

  public function getPath(string $path, string $lang = null)
  {
    $lang = $lang ?: $this->currentLang['short'];
    if (array_key_exists($path, $this->paths)) {
      return str_replace('//', '/', implode('/', [$this->languages[$lang]['route'], $this->paths[$path]['routes'][$lang]]));
    }

    //TODO return 404
    return null;
  }

  public function getTitle(string $route = null)
  {
    if ($route) {
      $t = $this->paths['route']['title'];
    }
    $t = $this->paths[$this->currentRoute]['title'][$this->currentLang['short']];
    if (!empty($t)) {
      return ' | ' . $t;
    }
    return null;
  }

  public function loadTemplate()
  {
    $templateFile = self::TEMPLATES_DIR . $this->paths[$this->currentRoute]['template'];
    if (file_exists($templateFile)) {
      return $templateFile;
    } else {
      throw new Exception('template file "' . $this->paths[$this->currentRoute]['template'] . '" not found');
    }
  }

  public function loadPart(string $partName)
  {
    $partFile = self::PARTS_DIR . $partName . '.php';
    if (file_exists($partFile)) {
      return $partFile;
    }
    throw new Exception('part file "' . $partName . '" not found');
  }
}
