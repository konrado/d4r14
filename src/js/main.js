[].forEach.call(
    document.querySelectorAll('.waypoint'),
    function (el) {
        var w = new Waypoint({
            element: el,
            handler: function () {
                w.destroy();
                this.element.className += ' animate';
            },
            offset: '80%'
        });
    }
);
