<?php

return [
  'title' => '{{TITLE}}',
  'branch' => '{{BRANCH}}',
  'hash' => '{{HASH}}',
  'beta' => '{{BETA}}',
  'google_maps_api_key' => '',
  'debug_config' => [
    'log_path' => '', //path to log file
    'debug_level' => 100, //levels described in \vendor\monolog\monolog\src\Monolog\Logger.php
  ]
];
