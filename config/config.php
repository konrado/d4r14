<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 26.02.2018
 * Time: 21:52
 */

return [
  'default_language' => 'pl',
  'languages' => [
    'pl' => [
      'route' => '/',
      'culture' => 'pl-PL',
      'short' => 'pl',
      'label' => 'pl'
    ]
  ],
  'paths' => [
    '_home' => [
      'template' => 'home.php',
      'title' => [
        'pl' => 'przełączniki, gniazda, przyciski'
      ],
      'routes' => [
        'pl' => ''
      ]
    ],
    '404' => [
      'template' => '404.php',
      'title' => [
        'pl' => 'Nie znaleziono'
      ],
      'routes' => [
        'pl' => null
      ]
    ]
  ]
];